from django.urls import path
from projects.views import project_view, show_project, create_project

urlpatterns = [
    path("create/", create_project, name="create_project"),
    path("<int:id>/", show_project, name="show_project"),
    path("", project_view, name="list_projects"),
]
