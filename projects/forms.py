from django.forms import ModelForm
from tasks.models import Project


class CreateForm(ModelForm):
    class Meta:
        model = Project
        fields = [
            "name",
            "description",
            "owner",
        ]
