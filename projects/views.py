from django.shortcuts import redirect, render, get_object_or_404
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import CreateForm

# Create your views here.


@login_required
def project_view(request):
    pv = Project.objects.filter(owner=request.user)
    context = {"pv": pv}
    return render(request, "projects/projects.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id, owner=request.user)
    context = {
        "project": project,
    }

    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = CreateForm()

    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
